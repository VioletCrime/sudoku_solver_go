package main

import (
    "encoding/json"
    "log"
    "net/http"
    "net/http/httputil"

    "github.com/gorilla/mux"
    "sudoku_solver_go/sudsol"
)

type Input struct {
    Board_str   string   `json:"board_str"`
}

func PostSolverEndpoint(w http.ResponseWriter, req *http.Request) {
    requestDump, err := httputil.DumpRequest(req, true)
    if err != nil {
        log.Println(err)
    }
    log.Println(string(requestDump))
    var input Input
    _ = json.NewDecoder(req.Body).Decode(&input)
    log.Println("input:", input)
    // Empty logfile name, ERROR logging, no pretty output, no stdout, skip logging
    mgr := sudsol.BuildManager(input.Board_str, "", 0, false, false, true)
    result := mgr.Run()
    json.NewEncoder(w).Encode(result)
}

func main() {
    router := mux.NewRouter()
    router.HandleFunc("/", PostSolverEndpoint).Methods("POST")
    log.Fatal(http.ListenAndServe(":12345", router))
}
