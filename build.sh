#!/bin/bash

set -e

readonly PROGNAME=$(basename $0)
readonly PROGDIR=$(readlink -m $(dirname $0))
readonly ARGS="$@"

readonly DOCKER_CMD="./docker-helper/run_with_docker.sh -i go19 -g "
readonly PUSH="pushd . &&"
readonly POP="&& popd"
readonly SSDIR="sudoku_solver_go"
readonly BUILD_PKG_CMD="$PUSH cd $SSDIR/sudsol && go install $POP"
readonly BUILD_CLI_CMD="$PUSH cd $SSDIR/cli && go build $POP"
readonly BUILD_API_CMD="$PUSH cd $SSDIR/api && go build $POP"
readonly DOCKER_TEST_CMD="$PUSH cd $SSDIR/sudsol && go test $POP"

readonly ARTIFACTS="api/api cli/cli"

usage() {
    cat <<- EOF
Usage: $PROGNAME [OPTION]

Options:
  -h, --help              Show this message
  -a, --all               Build all targets
  -c, --clean             Remove build artifacts
  -d, --docker            Build REST API docker container, including dependencies (pkg, rest)
  -c, --cli               Build CLI binary, including dependencies (pkg)
  -r, --rest              Build REST API binary, including dependencies (pkg)
  -t, --test              Run Tests
  -x, --debug             Enable build debugging
EOF
}

parse_args(){
    options=`getopt -o hadcrtxz --long help,all,docker,cli,rest,test,debug,clean -n "$PROGNAME" -- "$@"`
    eval set -- $options
    while true
    do
        case $1 in
            -h|--help)
                usage ; exit 0 ;;
            -a|--all)
                FLAG_DOCKER=true ; FLAG_CLI=true ; shift ;;
            -d|--docker)
                FLAG_DOCKER=true ; shift ;;
            -c|--cli)
                FLAG_CLI=true ; shift ;;
            -r|--rest)
                FLAG_API=true ; shift ;;
            -t|--test)
                TEST=true ; shift ;;
            -x|--debug)
                DEBUG=true ; shift ;;
            -z|--clean)
                CLEAN=true ; shift ;;
            --)
                break ;;
        esac
    done
    set +x
}

cleanup(){
    pushd .
    for ART in $ARTIFACTS; do
        if [ -e $ART ]; then
            rm -f $ART
        fi
    done

    # Docker cleanup cmds might return non-zero; shouldn't break script
    set +e
    docker rm $(docker ps -a -q) > /dev/null 2>&1
    docker rmi $(docker images | grep "^<none>" | awk '{print $3}') > /dev/null 2>&1
    set -e
}

run_tests(){
    $DOCKER_CMD "$DOCKER_TEST_CMD"
}

main(){
    local FLAG_DOCKER=false
    local FLAG_CLI=false
    local FLAG_API=false
    local CLEAN=false
    local DEBUG=false
    local TEST=false

    parse_args $ARGS

    if $DEBUG; then
        set -x
    fi

    if $CLEAN; then
        cleanup
    fi

    if $TEST; then
        run_tests
    fi

    # Build libs / bins if specified
    if $FLAG_CLI || $FLAG_API || $FLAG_DOCKER; then
        CMD="$DOCKER_CMD $BUILD_PKG_CMD"
        if $FLAG_CLI; then
            CMD="$CMD && $BUILD_CLI_CMD"
        fi
        if $FLAG_API || $FLAG_DOCKER; then
            CMD="$CMD && $BUILD_API_CMD"
        fi
        echo "CMD: $CMD"
        $CMD
    fi

    if $FLAG_DOCKER; then
        #bash $PROGDIR/docker-helper/build/make_go_sudoku_service.sh
        echo "TODO: Dockerize service"
    fi
    set +x
}

main
