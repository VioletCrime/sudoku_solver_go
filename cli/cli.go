// Notes go here

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"sudoku_solver_go/sudsol"
)

func main() {
	file := flag.String("file", "", "Filename containing sudoku board. See .board.example for details")
	inputStr := flag.String("board", "", "A non-delimited string representing a sudoku board, left-to-right, top-to-bottom")
	logFile := flag.String("logfile", "", "Custom location to log runtime events.")
	logLevel := flag.Int("loglevel", 0, "0: ERROR, 1: WARN, 2: DEBUG, 3: INFO, 4: XTREME")
	pretty := flag.Bool("p", false, "Make the output pretty. False by default.")
	toScreen := flag.Bool("t", false, "Output logging to stdout as well as the logfile.")
	skipLog := flag.Bool("n", false, "Skip logfile creation.")
	flag.Parse()
	var boardStr string = getStringForInputs(*file, *inputStr)
	mgr := sudsol.BuildManager(boardStr, *logFile, *logLevel, *pretty, *toScreen, *skipLog)
	result := mgr.Run()
	fmt.Println("Result:", result.Board, "solution took", result.Time, "to compute.")
}

func getStringForInputs(f string, raw_str string) string {
	var result string
	if raw_str != "" && f != "" { // Error case: both inputs are specified
		fmt.Println("Invalid command line arguments. Pass either board string or filename, not both.")
		os.Exit(3)
	} else if raw_str == "" && f == "" { // Error case: neither input was specified
		fmt.Println("Invalid command line arguments. Neither a board string nor filename were specified.")
		os.Exit(4)
	} else if raw_str != "" { //
		result = sanitizeCSVString(raw_str)
	} else if f != "" {
		result = getSanitizedFileContents(f)
	} else {
		fmt.Println("This error should be unreachable. May FSM have mercy on your heathen soul.")
		os.Exit(37)
	}
	if len(result) < 81 {
		fmt.Println("Board too short!")
		os.Exit(5)
	} else if len(result) > 81 {
		fmt.Println("WARN: Board is longer than needed. Truncating to 81 elements.")
		result = result[0:81]
	}
	return result
}

func getSanitizedFileContents(f string) string {
	in, err := ioutil.ReadFile(f)
	if err != nil {
		panic(err)
	}
	input := string(in)
	input = strings.Join(strings.Split(input, "\n"), "") // drop the last newline
	input = strings.Join(strings.Split(input, " "), "")  // replace spaces with commas
	//fmt.Printf("Standardized string:\n%s\n\n", input)  // debug helper
	return sanitizeCSVString(input) // Input is now standardized. Go get 'em, tiger!
}

func sanitizeCSVString(s string) string {
	elements := strings.Split(s, "")
	for i := range elements {
		if !isBoardElemValid(elements[i]) {
			elements[i] = "0"
		}
	}
	retval := strings.Join(elements, "")
	return retval
}

func isBoardElemValid(s string) bool {
	valid_entries := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}
	for i := range valid_entries {
		if s == valid_entries[i] {
			return true
		}
	}
	return false
}
