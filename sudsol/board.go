package sudsol

import (
	"strings"
)

type board struct {
	brd        [][]square
	numUnknown int
	knownMoves [][]int
    logger     logger
}

func buildBoardFromString(s string, l logger) *board {
	newBoard := [][]square{}
	vals := strings.Split(s, "")
	for i := 0; i < 9; i++ {
		newRow := []square{}
		for j := 0; j < 9; j++ {
			sq := square{vals[0], []string{}}
			newRow = append(newRow, sq)
			vals = vals[1:]
		}
		newBoard = append(newBoard, newRow)
	}
	return &board{newBoard, 0, make([][]int, 0), l}
}

func (b *board) copyBoard() *board {
    newbrd := [][]square {}
    for _, line := range(b.brd){
        newline := []square {}
        for _, src_sq := range(line) {
            newpvals := []string {}
            for _, pval := range(src_sq.pvals) {
                newpvals = append(newpvals, pval)
            }
            newline = append(newline, square{src_sq.value, newpvals})
        }
        newbrd = append(newbrd, newline)
    }
	retval := board{newbrd, b.numUnknown, b.knownMoves, b.logger}
	return &retval
}

func (b *board) formatBoard(indent int, pretty bool) string {
	if pretty {
		return b.formatBoardPretty(indent)
	}
	retval := ""
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			retval += b.brd[i][j].value
		}
	}
	return retval
}

func (b *board) formatBoardPretty(indent int) string {
	retval := "\n"
	pad := strings.Repeat(" ", indent)
	for i := 0; i < 9; i++ {
		retval += pad
		if i != 0 && i%3 == 0 {
			retval += "──────┼───────┼──────\n"
			retval += pad
		}
		for j := 0; j < 9; j++ {
			if j != 0 && j%3 == 0 {
				retval += "│ "
			}
			retval += b.brd[i][j].value
			retval += " "
		}
		retval += "\n"
	}
	return retval
}

func (b *board) formatPossibilities(indent int) string {
	retval := "\n"
	pad := strings.Repeat(" ", indent)
	for i := 0; i < 9; i++ {
		if i != 0 && i%3 == 0 {
			retval += pad
			retval += "━━━━━┿━━━━━┿━━━━━╋━━━━━┿━━━━━┿━━━━━╋━━━━━┿━━━━━┿━━━━━\n"
		} else if i != 0 {
			retval += pad
			retval += "─────┼─────┼─────╂─────┼─────┼─────╂─────┼─────┼─────\n"
		}
		retval += getPossibilitiesForRow(b.brd[i], pad)
	}
	return retval
}

func getPossibilitiesForRow(s []square, pad string) string {
	retVal := ""
	rng1 := []string{"1", "2", "3"}
	rng2 := []string{"4", "5", "6"}
	rng3 := []string{"7", "8", "9"}
	rngs := [][]string{rng1, rng2, rng3}
	for _, rng := range rngs {
		retVal += pad
		retVal += " "
		for i := 0; i < 9; i++ {
			for j, val := range rng {
				nextVal := "-"
				if valIsPossible(s[i].pvals, val) {
					nextVal = val
				}
				retVal += nextVal
				if j == 2 && (i+1)%3 != 0 {
					retVal += " │ "
				}
			}
			if (i+1)%3 == 0 && i != 8 {
				retVal += " ┃ "
			}
		}
		retVal += " \n"
	}
	return retVal
}

func (b *board) processBoardState() {
	b.logger.log(4, 2, "Entering processBoardState()")
	for i := 0; i < 9; i++ { // Iterate rows
		for j := 0; j < 9; j++ { //Iterate columns
			if b.brd[i][j].value == "0" { // If the value is unknown, we need to narrow what it could be
				b.numUnknown++            // Count the number of unknowns to track progress
				for k := 1; k < 10; k++ { // Iterate potential values
					val := intToStr(k)
					valConnected := b.valInRow(i, j, val) || b.valInCol(i, j, val) || b.valInSubgrid(i, j, val)
					if !valConnected {
						b.brd[i][j].pvals = append(b.brd[i][j].pvals, intToStr(k))
					}
				}
				if len(b.brd[i][j].pvals) == 1 {
					newMove := []int{i, j, strToInt(b.brd[i][j].pvals[0])}
					b.knownMoves = append(b.knownMoves, newMove)
				}
			}
		}
	}
	b.logger.log(4, -2, "Exiting processBoardState()")
}

func (b *board) valInRow(i int, j int, v string) bool {
	b.logger.log(4, 2, "Entering valInRow(%v, %v, %v)", i, j, v)
	retVal := false
	for col := 0; col < 9; col++ {
		if col != j {
			if b.brd[i][col].value == v {
				b.logger.log(3, 0, "Row match for %v([%v][%v]) found at [%v][%v].", v, i, j, i, col)
				retVal = true
				break
			}
		}
	}
	b.logger.log(4, -2, "Exiting valInRow() returns %v(%T)", retVal, retVal)
	return retVal
}

func (b *board) valInCol(i int, j int, v string) bool {
	b.logger.log(4, 2, "Entering valInCol(%v, %v, %v)", i, j, v)
	retVal := false
	for row := 0; row < 9; row++ {
		if row != i {
			if b.brd[row][j].value == v {
				b.logger.log(3, 0, "Column match for %v([%v][%v]) found at [%v][%v].", v, i, j, row, j)
				retVal = true
				break
			}
		}
	}
	b.logger.log(4, -2, "Exiting valInCol() returning %v(%T)", retVal, retVal)
	return retVal
}

func (b *board) valInSubgrid(i int, j int, v string) bool {
	b.logger.log(4, 2, "Entering valInSubgrid(%v, %v, %v)", i, j, v)
	retVal := false
	startI, endI, startJ, endJ := getSubGrid(i, j)
	for row := startI; row <= endI; row++ {
		for col := startJ; col <= endJ; col++ {
			if row != i && col != j {
				if b.brd[row][col].value == v {
					b.logger.log(4, 0, "Subgrid match for %v([%v][%v]) found at [%v][%v].", v, i, j, row, col)
					retVal = true
					break
				}
			}
		}
	}
	b.logger.log(3, -2, "Exiting valInSubgrid(%v, %v, %v) returning %v", i, j, v, retVal)
	return retVal
}

func (b *board) makeMove(i int, j int, val string) {
	b.logger.log(4, 2, "makeMove() Writing value %v to [%v,%v].", val, i, j)
    b.brd[i][j].value = val
    b.brd[i][j].pvals = []string {}
    b.updatePossibleVals(i, j, val)
    b.numUnknown--
    b.logger.log(4, -2, "Exiting makeMove()")
}

func (b *board) placeKnowns() {
	b.logger.log(3, 2, "Entering placeKnowns(). There are %v known values to place", len(b.knownMoves))
	for len(b.knownMoves) > 0 {
		move := b.knownMoves[0]
		b.knownMoves = b.knownMoves[1:]
		i := move[0]
		j := move[1]
		val := intToStr(move[2])
		if b.brd[i][j].value != val {
            b.makeMove(i, j, val)
			b.numUnknown--
		}
	}
	b.logger.log(3, -2, "Exiting placeKnowns().")
}

func (b *board) updatePossibleVals(i int, j int, val string) {
	for k := 0; k < 9; k++ {
		if k != i {
			b.removePossibleVal(k, j, val) // Remove along the row
		}
		if k != j {
			b.removePossibleVal(i, k, val) //Remove along the column
		}
	}
	startI, endI, startJ, endJ := getSubGrid(i, j)
	for row := startI; row <= endI; row++ {
		if row == i { // We've already cleared along the row; skip this val
			continue
		}
		for col := startJ; col <= endJ; col++ {
			if col == j { // We're already cleared along the column...
				continue
			}
			b.removePossibleVal(row, col, val)
		}
	}
}

func (b *board) removePossibleVal(i int, j int, val string) {
	valIndex := indexOfValue(b.brd[i][j].pvals, val)
	if valIndex != -1 {
		b.brd[i][j].pvals = append(b.brd[i][j].pvals[0:valIndex], b.brd[i][j].pvals[valIndex+1:]...)
	}
}

func (b *board) findSinglePossibilities() bool {
	b.logger.log(4, 2, "Entering findSinglePossibilities().")
	retval := false
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if len(b.brd[i][j].pvals) == 1 {
				b.logger.log(4, 0, "Single possibility found at [%v, %v] (%v).", i, j, b.brd[i][j].pvals)
				retval = true
				newMove := []int{i, j, strToInt(b.brd[i][j].pvals[0])}
				b.knownMoves = append(b.knownMoves, newMove)
			}
		}
	}
	b.logger.log(3, -2, "Exiting findSinglePossibilities() returning %v.", retval)
	return retval
}

func (b *board) validateSolution() bool {
	b.logger.log(4, 2, "Entering validateSolution().")
	retVal := b.validateRowsAndCols() && b.validateSubgrid()
	b.logger.log(3, -2, "Exiting validateSolution() returning %v.", retVal)
	return retVal
}

func (b *board) validateRowsAndCols() bool {
	b.logger.log(4, 2, "Entering board.validateRows().")
	retval := true
	for i := 0; i < 9; i++ {
        for k := 1; k < 10; k++ {
			kStr := intToStr(k)
			rowFoundK := false
			colFoundK := false
			for j := 0; j < 9; j++ {
				if b.brd[i][j].value == kStr {
					rowFoundK = true
				}
				if b.brd[j][i].value == kStr {
					colFoundK = true
				}
			}
            if rowFoundK == false || colFoundK == false {
                retval = false
                break
            }
		}
		if retval == false { // save a few cycles in the fail case
			break
		}
	}
	b.logger.log(3, -2, "Exiting board.validateRows() returning %v.", retval)
	return retval
}

func (b *board) validateSubgrid() bool {
	b.logger.log(4, 2, "Entering board.validateSubgrid().")
	retval := true
	subGridIndices := []int {0, 3, 6}
	for _, si := range(subGridIndices) {
		for _, sj := range(subGridIndices) {
			starti, endi, startj, endj := getSubGrid(si, sj)
			for k := 1; k < 10; k++ {
				kStr := intToStr(k)
				foundK := false
				for i := starti; i <= endi; i++ {
					for j := startj; j <= endj; j++ {
						if b.brd[i][j].value == kStr {
							foundK = true
							break
						}
					}
				}
				if foundK == false {
					retval = false
					break
				}
				if retval == false {
					break
				}
			}
			if retval == false {
				break
			}
		}
		if retval == false {
			break
		}
	}
	b.logger.log(3, -2, "Exiting board.validateSubgrid() returning %v.", retval)
	return retval
}

func (b *board) isStateValid() bool {
	b.logger.log(4, 2, "Entering isStateValid().")
	retval := true
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if b.brd[i][j].value == "0" {
				if len(b.brd[i][j].pvals) < 1 {
					retval = false
					break
				}
			}
		}
	}
	b.logger.log(3, -2, "Exiting isStateValid() returning %v.", retval)
	return retval
}

func (b *board) minOptions() (int, int) {
	b.logger.log(4, 2, "Entering minOptions().")
	minI := 0
	minJ := 0
	minLen := 10
	for i := 0; i < 9; i++ {
		for j := 0; j < 9; j++ {
			if b.brd[i][j].value == "0" && len(b.brd[i][j].pvals) > 0 && len(b.brd[i][j].pvals) < minLen {
				minI = i
				minJ = j
				minLen = len(b.brd[i][j].pvals)
			}
		}
	}
	b.logger.log(3, -2, "Exiting minOptions() returning %v, %v (%v options).", minI, minJ, minLen)
	return minI, minJ
}
