package sudsol

import "time"

type manager struct {
	board_str string
	logger    logger
	pretty    bool
}

type bfsresult struct {
    board    *board
    valid    bool
}

type Result struct {
    Board    string
    Time     string
}

func BuildManager(b_str string, logf string, logl int, p bool, t bool, n bool) *manager {
	logger := createLogger(logf, int8(logl), t, n)
	manager := manager{b_str, logger, p}
	return &manager
}

func (m manager) Run() *Result{
	m.logger.log(3, 0, "Entering manager.Run().")
	board := buildBoardFromString(m.board_str, m.logger)
	board.processBoardState()
	m.logger.log(3, 0, "Initial board processing complete. Found %v new values to place.", len(board.knownMoves))
	board.placeKnowns()

    t1 := time.Now()
	//m.logic()
    bfs_result := bfs(board)
	t2 := time.Now()
	diff := t2.Sub(t1)
    result := Result{bfs_result.board.formatBoard(0, m.pretty), diff.String()}
	m.logger.log(2, 0, "Solution took %v seconds.", diff.Seconds())
	m.logger.log(3, 0, "Exiting manager.Run().")

	return &result
}

//func (m manager) combined()

func logic(b *board) {
	/*
	Run pure-logic algorithms to simplify / solve the board.
	Returns true if the board is solved upon exit.
	*/
	b.logger.log(4, 2, "Entering logic()")
	progressMade := true
	for progressMade {
		resOne := b.findSinglePossibilities()
		resTwo := b.isolatedPossibilities()
		resTre := b.nakedTwins()
		progressMade = resOne || resTwo || resTre
		if progressMade {
			b.placeKnowns()
		}
	}
	b.logger.log(3, -2, "Exiting logic()")
}

func bfs(b *board) *bfsresult{
    b.logger.log(3, 2, "Entering BFS()")
    defer b.logger.log(3, -2, "Exiting BFS()")
    logic(b)
	if b.validateSolution() {
        res := bfsresult{b, true}
		return &res
	} else if !b.isStateValid() {
        res := bfsresult{b, false}
		return &res
	} else {
		row, col := b.minOptions()
		var guesses []*board
		for _, val := range(b.brd[row][col].pvals) {
			newBoard := b.copyBoard()
            newBoard.makeMove(row, col, val)
			guesses = append(guesses, newBoard)
		}
		for _, guess := range(guesses) {
			guess_result := bfs(guess)
            if guess_result.valid{
                return guess_result
            }
		}
	}
    b.logger.log(0, 0, "DROPPED THROUGH BFS() LOGIC! YOU SHOULD NEVER SEE THIS!!")
    res := bfsresult{b, false}
    return &res
}
